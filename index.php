<?php 
require 'animal.php';
require 'frog.php';
require 'ape.php';

$sheep = new Animal("shaun");
echo "Name: $sheep->name <br>"; // "shaun"
echo "legs: $sheep->legs <br>"; // 4
echo "cold blooded: $sheep->cold_blooded"; // "no"

echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name: $kodok->name <br>"; 
echo "legs: $kodok->legs <br>"; 
echo "cold blooded: $kodok->cold_blooded <br>";
echo "Jump: " . $kodok->jump(); // "hop hop"

echo "<br><br>";

$sungkong = new Ape("kera sakti");
echo "Name: $sungkong->name <br>"; 
echo "legs: $sungkong->legs <br>"; 
echo "cold blooded: $sungkong->cold_blooded <br>";
echo "Jump: " . $sungkong->yell(); // "hop hop"



